package layerAssignment;
import java.util.Scanner;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;


/**
 * Hello world!
 *
 */
public class App 
{
    private static final String USER_AGENT = "Mozilla/5.0";
    public static void main( String[] args ) throws IOException
    {
        Scanner myInput = new Scanner(System.in);
        int choice = 0;
        String institution = "";
        String fname = "";
        String lname = "";
        System.out.println("Enter you first name, last namea institution.");
        System.out.println("Hint: Input 1 for TAMK, 2 for TAU, 3 for Normaalikoulu.");
        System.out.println("Input sample:");
        System.out.println("Tim");
        System.out.println("Cook");
        System.out.println("1");
        System.out.println("The input above means a person whose name is Tim Cook studying in TAMK");
        fname = myInput.nextLine();
        lname = myInput.nextLine();
        choice = myInput.nextInt();
        switch(choice){
            case 1: institution = "TAMK";
            break;
            case 2: institution = "TAU";
            break;
            case 3: institution = "Normaalikoulu";
            break;
            default: break;
        }
        String emailString = "http://localhost:1111/" + institution + "/" + fname + "/" + lname;
        URL emailRequestUrl = new URL(emailString);
        HttpURLConnection emailCon = (HttpURLConnection) emailRequestUrl.openConnection();
		emailCon.setRequestMethod("GET");
		emailCon.setRequestProperty("User-Agent", USER_AGENT);
		int emailResponseCode = emailCon.getResponseCode();
		System.out.println("GET Response Code :: " + emailResponseCode);
		if (emailResponseCode == HttpURLConnection.HTTP_OK) {
			BufferedReader in = new BufferedReader(new InputStreamReader(
                emailCon.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			// print result
            System.out.println("User email address is successfully regestered.");
			System.out.println(response.toString());
		} else {
			System.out.println("GET request not worked");
		}

        String idString = "http://localhost:2222/" + institution;
        URL idRequestUrl = new URL(idString);
        System.out.println(idString);
        HttpURLConnection idCon = (HttpURLConnection) idRequestUrl.openConnection();
		idCon.setRequestMethod("GET");
		idCon.setRequestProperty("User-Agent", USER_AGENT);
		int idResponseCode = idCon.getResponseCode();
		System.out.println("GET Response Code :: " + idResponseCode);
		if (idResponseCode == HttpURLConnection.HTTP_OK) {
			BufferedReader in = new BufferedReader(new InputStreamReader(
                idCon.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
            System.out.println("User ID is successfully regestered.");
			System.out.println(response.toString());
		} else {
			System.out.println("GET request not worked");
		}
    }
}
