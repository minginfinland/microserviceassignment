# Basic Spring Boot Rest Service

IDM microservice app

To run you need  
Java 11 (minimum 8)  
Maven  
Git  

## Package instruction
- emailServer: server for generating email address
- idServer: server for generating an random number of ID, with an institution tag
- apiLayer: api layer app

## Present layer installation
````
cd presentLayer; mvn package
````

## email server installation
````
cd emailserver; mvn install
````

## ID server installation
````
cd idServer; mvn install
````

## Start the service
````
cd emailserver; mvn spring-boot:run
cd idServer; mvn spring-boot:run
````

## uninstallation
````
cd emailserver; mvn clean
cd idServer; mvn clean
cd presentLayer; mvn clean
````